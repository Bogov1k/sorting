﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorting
{
    class Program
    {
        static void Main(string[] args)
        {
            
            int[] numbers = new int[] { 1, 14, 6, 4, 23, 4, 8, 32, 5, 7, 43, 20, 12, 89, 54, 47, 45, 44, 78 };
            
                // Массив от пользователя
            //int[] numbers = new int[16];
            //Console.ForegroundColor = ConsoleColor.DarkMagenta;
            //Console.WriteLine("Введите шестнадцать чисел:");
            //for (int i = 0; i < numbers.Length; i++)
            //{
            //    Console.Write("{0}-е число: ", i + 1);
            //    numbers[i] = Int32.Parse(Console.ReadLine());
            //}




            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\n-----------  Массив чисел  -----------\n");
            for (int k = 0; k < numbers.Length; k++)
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write(numbers[k] + " ");
            }

            var leftNumbers = new List<int>();
            var rightNumbers = new List<int>();

            foreach (var i in numbers)
                if (i % 2 == 1)
                {
                    leftNumbers.Add(i);
                }
                else
                {
                    rightNumbers.Add(i);
                }

            // Вывод нечётных чисел
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\n\n----------- Нечётные числа -----------\n");
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            var _leftNumbers = leftNumbers.ToArray();
            
            foreach (var i in _leftNumbers)
                Console.Write($"{i.ToString()} ");

            // Сортировка нечётных чисел
            int temp;
            for (int i = 0; i < _leftNumbers.Length - 1; i++)
            {
                for (int j = i + 1; j < _leftNumbers.Length; j++)
                {
                    if (_leftNumbers[i] > _leftNumbers[j])
                    {
                        temp = _leftNumbers[i];
                        _leftNumbers[i] = _leftNumbers[j];
                        _leftNumbers[j] = temp;
                    }
                }
            }

            // Вывод нечётных чисел по возрастанию
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\n\n--- Нечётные числа по возрастанию  ---\n");
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            for (int i = 0; i < _leftNumbers.Length; i++)
            {
                Console.Write(_leftNumbers[i] + " ");
            }
            Console.ForegroundColor = ConsoleColor.Red;

            // Вывод чётных чисел
            Console.WriteLine("\n\n-----------  Чётные числа  -----------\n");
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            var _rightNumbers = rightNumbers.ToArray();
            foreach (var i in _rightNumbers)
                Console.Write($"{i.ToString()} ");

            // Сортировка чётных чисел
            int temp2;
            for (int i = 0; i < _rightNumbers.Length - 1; i++)
            {
                for (int j = i + 1; j < _rightNumbers.Length; j++)
                {
                    if (_rightNumbers[i] > _rightNumbers[j])
                    {
                        temp2 = _rightNumbers[i];
                        _rightNumbers[i] = _rightNumbers[j];
                        _rightNumbers[j] = temp2;
                    }
                }
            }


            // Вывод чётных чисел по возрастанию
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\n\n---- Чётные числа по возрастанию  ----\n");
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            for (int i = 0; i < _rightNumbers.Length; i++)
            {
                Console.Write(_rightNumbers[i] + " ");
            }
            Console.ForegroundColor = ConsoleColor.Red;

            
            // Вывод чисел массива по возрастанию в два столбца 


            // Заведомо принимаем, что массив нечётных чисел длиннее, если наоборот, то меняем местами массивы
            if (_leftNumbers.Length < _rightNumbers.Length)
            {
                var tmp = _leftNumbers;
                _leftNumbers = _rightNumbers;
                _rightNumbers = tmp;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\n\n------ Чётные ----- Нечётные ---------");
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\n\n------ Нечётные ----- Чётные ---------");
                Console.ForegroundColor = ConsoleColor.DarkBlue;
            }

            // Вывод элементов массива до конца правого массива
            for (int z = 0; z < _rightNumbers.Length; z++)
            {
                Console.Write("\n\t" + _leftNumbers[z] + "\t\t" + _rightNumbers[z]);
            }

            // Довыводим массив в левый столбец
            for (int z = _rightNumbers.Length; z < _leftNumbers.Length; z++)
            {
                Console.Write("\n\t" + _leftNumbers[z]);
            }

            Console.ReadKey();

        }
    }
}

/* проверка */